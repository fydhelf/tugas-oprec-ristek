from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('desc/', views.desc, name='desc'),
    path('data/', views.data, name='data'),
    path('like/', views.Like, name='Like'),
    path('book/', views.Book, name='Book'),
]
