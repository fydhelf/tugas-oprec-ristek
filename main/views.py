import json
import re
from django.shortcuts import render
from pip._vendor import requests
from django.http.response import JsonResponse

def home(request):
    response = requests.get("http://www.omdbapi.com/?i=tt3896198&apikey=c80fffae").json()
    return render(request, 'main/home.html', {'response':response})

def desc(request):
    response = requests.get("http://www.omdbapi.com/?i=tt3896198&apikey=c80fffae").json()
    return render(request,'main/desc.html', {'response':response})

def data(request):
    response = requests.get("http://www.omdbapi.com/?i=tt3896198&apikey=c80fffae").json()
    url=request.GET['q']
    if not re.search(url, response['Title'], flags=re.IGNORECASE):
        response = 'Not Found'
    data=response
    return JsonResponse(data,safe=False)

def Like(request):
    response = requests.get("http://www.omdbapi.com/?i=tt3896198&apikey=c80fffae").json()
    return render(request,'main/Like.html', {'response':response})

def Book(request):
    response = requests.get("http://www.omdbapi.com/?i=tt3896198&apikey=c80fffae").json()
    return render(request,'main/Book.html', {'response':response})